#!python3.6
import os
import sys
import shutil

BASE = os.path.abspath(os.path.dirname(__file__))

DATA = os.path.join(BASE, "data")
BIN = os.path.join(BASE, "bin")

FILES = [
    "third-party/nanomsg/bin/nanomsg.dll",
    "../../../../software/vc/of_v0.9.8_vs_release/export/vs/x64/assimp.dll",
    "../../../../software/vc/of_v0.9.8_vs_release/export/vs/x64/fmodex64.dll",
    "../../../../software/vc/of_v0.9.8_vs_release/export/vs/x64/fmodexL64.dll",
    "../../../../software/vc/of_v0.9.8_vs_release/export/vs/x64/FreeImage.dll",
    "../../../../software/vc/of_v0.9.8_vs_release/export/vs/x64/freetype.dll",
    "../../../../software/vc/of_v0.9.8_vs_release/export/vs/x64/glut32.dll",
    "../../../../software/vc/of_v0.9.8_vs_release/export/vs/x64/libeay32.dll",
    "../../../../software/vc/of_v0.9.8_vs_release/export/vs/x64/ssleay32.dll",
    "../../../../software/vc/of_v0.9.8_vs_release/export/vs/x64/Zlib.dll",

    "third-party/CrashRpt_v.1.4.3_r1645/build/bin/x64/CrashSender1403.exe",
    "third-party/CrashRpt_v.1.4.3_r1645/bin/crashrpt_lang.ini",
]

def write_python_version():
    with open(os.path.join(BASE, "python.version"), "wb") as outf:
        outf.write(sys.version.encode("ascii"))

def main():
    assert os.path.exists(BIN)
    data_dest = os.path.join(BIN, "data")
    if not os.path.exists(data_dest):
        shutil.copytree(DATA, data_dest)

    for f in FILES:
        fp = os.path.join(BASE, f)
        assert os.path.exists(fp), fp
        dest = os.path.join(BIN, os.path.basename(fp))
        if not os.path.exists(dest):
            shutil.copy(fp, dest)

if __name__ == '__main__':
    main()
