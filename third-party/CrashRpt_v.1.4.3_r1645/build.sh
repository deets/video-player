#!/bin/bash
export PATH=$PATH:"/c/Program Files (x86)/MSBuild/14.0/Bin"

if [ ! -e build ]; then
    mkdir build
fi

cd build

cmake .. -G"Visual Studio 14 2015 Win64" -DCRASHRPT_BUILD_SHARED_LIBS=OFF -DCRASHRPT_LINK_CRT_AS_DLL=ON
msbuild.exe CrashRpt.sln -p:Configuration=Release
