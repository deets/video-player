#pragma once

#include "player.hpp"

#include "ofMain.h"

#include <json/json.h>

class Calibrator
{
public:
  Calibrator(Player&);

  void draw();
  void keyPressed(int key);
  void keyReleased(int key);
  void save_configuration() const;
  void load_configuration();

private:
  void draw_marker(int x, int y);
  void next();
  void prev();
  std::size_t size() const;

  const ofVec3f& vertex() const;
  ofVec3f& vertex();

  const ofMesh& mesh() const;
  ofMesh& mesh();

  void left();
  void right();
  void up();
  void down();

  float step() const;

  std::wstring data_folder() const;
  std::wstring calibration_filename() const;
  Json::Value mesh2configuration() const;

  Player& _player;
  ofImage _marker;

  std::size_t _marked_vertex;

  bool _shift;
};
