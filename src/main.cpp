#include "ofMain.h"
#include "ofApp.h"

#include "optionparser.h"

#include "CrashRpt.h"

namespace {

enum OptionIndex { AUTO_FOREGROUND, FULLSCREEN, FADEOUT };
enum OptionType { ENABLE, FLOAT };


struct Arg {
   static void printError(const char* msg1, const option::Option& opt, const char* msg2)
   {
     fprintf(stderr, "ERROR: %s", msg1);
     fwrite(opt.name, opt.namelen, 1, stderr);
     fprintf(stderr, "%s", msg2);
   }

  static option::ArgStatus Numeric(const option::Option& option, bool msg)
  {
    char* endptr = 0;
    if (option.arg != 0 && std::strtof(option.arg, &endptr)){};
    if (endptr != option.arg && *endptr == 0)
      return option::ARG_OK;
    if (msg) printError("Option '", option, "' requires a numeric argument\n");
    return option::ARG_ILLEGAL;
   }
};

const option::Descriptor USAGE[] = {
  { AUTO_FOREGROUND,                                   // index
    ENABLE,                                            // type
    "a",                                               // shortopt
    "auto-foreground",                                 // longopt
    option::Arg::None,                                         // check_arg
    "--auto-foreground - bring app to front in regular interval." // help
  },
  { FULLSCREEN,                                        // index
    ENABLE,                                            // type
    "f",                                               // shortopt
    "full-screen",                                     // longopt
    option::Arg::None,                                         // check_arg
    "--full-screen  Tells the program to start in full screen." // help
  },
  { FADEOUT,                                   // index
    FLOAT,                                            // type
    "o",
    "fadeout",                                 // longopt
    Arg::Numeric,                                // check_arg
    "--fadeout fadeout in seconds." // help
  },
  { 0, 0, 0, 0, 0, 0 } // sentinel
};

} // end ns anonymous

//========================================================================

int main(int argc, const char** argv )
{
  CR_INSTALL_INFO info;
  memset(&info, 0, sizeof(CR_INSTALL_INFO));
  info.cb = sizeof(CR_INSTALL_INFO);             // Size of the structure
  info.pszAppName = _T("MagicBook Video Player"); // App name
  info.pszAppVersion = _T("1.0.0");              // App version
  info.dwFlags = CR_INST_ALL_POSSIBLE_HANDLERS | CR_INST_DONT_SEND_REPORT | CR_INST_AUTO_THREAD_HANDLERS;

  auto nInstResult = crInstall(&info);
  assert(nInstResult==0);

  ofSetupOpenGL(1024, 768, OF_WINDOW);			// <-------- setup the GL context
  // this kicks off the running of my app
  // can be OF_WINDOW or OF_FULLSCREEN
  // pass in width and height too:
  ProjectionRect rect;
  auto w =  ofGetWidth();
  auto h = ofGetHeight();
  rect.topleft = ofPoint(-w / 5.0, -h / 5.0, 0);
  rect.topright = ofPoint(w / 5.0, -h / 5.0, 0);
  rect.bottomleft = ofPoint(-w / 5.0, h / 5.0, 0);
  rect.bottomright = ofPoint(w / 5.0, h / 5.0, 0);


  argc-=(argc>0); argv+=(argc>0); // skip program name argv[0] if present
  option::Stats  stats(USAGE, argc, argv);
  std::vector<option::Option> options(stats.options_max), buffer(stats.buffer_max);
  option::Parser parse(USAGE, argc, argv, options.data(), buffer.data());

  if (parse.error())
  {
     return 1;
  }
  const auto auto_foreground = options[AUTO_FOREGROUND];
  const auto fullscreen = options[FULLSCREEN];
  float fadeout = 0.0f;
  if(options[FADEOUT])
  {
    fadeout = strtof(options[FADEOUT].arg, NULL);
  }
  ofRunApp(new ofApp(rect, auto_foreground, fullscreen, fadeout));

  crUninstall(); // Uninstall exception handlers
}
