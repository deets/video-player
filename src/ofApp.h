#pragma once

#include "player.hpp"
#include "calibrator.hpp"

#include "ofMain.h"
#include "nn.hpp"

#include <json/json.h>
#include <chrono>

enum class Mode
{
  NORMAL,
  CALIBRATE
};


class ofApp : public ofBaseApp
{
 public:

  ofApp(const ProjectionRect&, bool auto_foreground, bool fullscreen, float fadeout);

  void setup();
  void update();
  void draw();

  void keyPressed(int key);
  void keyReleased(int key);
  void mouseMoved(int x, int y );
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);
  void mouseEntered(int x, int y);
  void mouseExited(int x, int y);
  void windowResized(int w, int h);
  void dragEvent(ofDragInfo dragInfo);
  void gotMessage(ofMessage msg);

 private:
  std::string receive_message();
  void process_json(const Json::Value&);
  void process_message();
  void drawCenteredText();
  void full_screen(bool);
  void auto_foreground();

  Mode _mode;

  ofColor _background;
  ofColor _text_color;

  nn::socket _socket;

  Player _player;
  Calibrator _calibrator;

  ofTrueTypeFont overlay_font;
  std::string _center_text;

  bool _is_fullscreen;
  bool _auto_foreground;
  std::chrono::time_point<std::chrono::steady_clock> _last_auto_foreground;
};
