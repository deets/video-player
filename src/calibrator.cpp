#include "calibrator.hpp"

#include <Shlobj.h>

#include <sstream>

Calibrator::Calibrator(Player& player)
  : _player(player)
  , _marked_vertex(0)
  , _shift(false)
{
  _marker.load("images/marker.png");
}


  // const auto& mesh = _player.mesh();
  // const auto v = mesh.getVertices()[0];
  // draw_marker(v.x, v.y);



void Calibrator::draw_marker(int x, int y)
{
  // same coordinate system as the player
  ofPushMatrix(); //Store the coordinate system
  // translate marker
  ofTranslate( -_marker.getWidth() / 2, -_marker.getHeight() / 2);
  ofSetColor(255, 255, 255, 255);
  _marker.draw(x, y);
  ofPopMatrix(); //Restore the coordinate system
}


void Calibrator::draw()
{
  ofPushMatrix(); //Store the coordinate system
  //Move the coordinate center to screen's center
  ofTranslate( ofGetWidth()/2, ofGetHeight()/2, 0 );
  mesh().draw();

  const auto v = vertex();
  draw_marker(v.x, v.y);

  ofPopMatrix(); //Restore the coordinate system
}


const ofMesh& Calibrator::mesh() const
{
  return _player.mesh();
}


ofMesh& Calibrator::mesh()
{
  return _player.mesh();
}


const ofVec3f& Calibrator::vertex() const
{
  return mesh().getVertices()[_marked_vertex];
}


ofVec3f& Calibrator::vertex()
{
  return mesh().getVertices()[_marked_vertex];
}


void Calibrator::keyPressed(int key)
{
  bool save = true;
  switch(key)
  {
  case 'n':
    next();
    break;
  case 'p':
    prev();
    break;
  case 356:
    left();
    break;
  case 357:
    up();
    break;
  case 358:
    right();
    break;
  case 359:
    down();
    break;
  case 2304:
    _shift = true;
    break;
  default:
    save = false;
    break;
  }
  if(save)
  {
    save_configuration();
  }
}


void Calibrator::keyReleased(int key)
{
  switch(key)
  {
    case 2304:
    _shift = false;
    break;
  }
}


float Calibrator::step() const
{
  return _shift ? 10.0 : 1.0;
}


void Calibrator::left()
{
  vertex() -= step() * ofVec3f(1, 0, 0);
}


void Calibrator::right()
{
  vertex() += step() * ofVec3f(1, 0, 0);
}


void Calibrator::up()
{
  vertex() -= step() * ofVec3f(0, 1, 0);
}


void Calibrator::down()
{
  vertex() += step() * ofVec3f(0, 1, 0);
}


std::size_t Calibrator::size() const
{
  return _player.mesh().getVertices().size();
}


void Calibrator::next()
{
  _marked_vertex = (_marked_vertex + 1) % size();
}


void Calibrator::prev()
{
  _marked_vertex = (_marked_vertex - 1) % size();
}


std::wstring Calibrator::data_folder() const
{
  PWSTR path;

  HRESULT hr = SHGetKnownFolderPath(
      FOLDERID_LocalAppData,
      0,
      NULL,
      &path
  );
  return std::wstring(path);
}


std::wstring Calibrator::calibration_filename() const
{
  std::wstringstream ss;
  ss << data_folder() << L"\\" << L"video-player-calibration.json";
  return ss.str();
}


Json::Value Calibrator::mesh2configuration() const
{
  Json::Value configuration(Json::objectValue);
  Json::Value mesh_json(Json::arrayValue);
  for(const auto v : mesh().getVertices())
  {
    Json::Value vertex(Json::arrayValue);
    vertex.append(v.x);
    vertex.append(v.y);
    mesh_json.append(vertex);
  }
  configuration["mesh"] = mesh_json;
  return configuration;
}


void Calibrator::save_configuration() const
{
  const auto config = mesh2configuration();

  std::ofstream outf(calibration_filename());
  Json::StyledWriter writer;
  const auto json = writer.write(config);
  std::wcout << "saving to " << calibration_filename() << "\n";
  outf << json;
}


void Calibrator::load_configuration()
{
  std::ifstream inf(calibration_filename());
  if(inf)
  {
    Json::Reader reader;
    Json::Value root(Json::nullValue);
    if(reader.parse(inf, root) && root.isMember("mesh"))
    {
      const auto mesh_json = root["mesh"];
      if(mesh_json.isArray())
      {
        auto& vertices = mesh().getVertices();
        if(mesh_json.size() == vertices.size())
        {
          for(std::size_t i=0; i < vertices.size(); ++i)
          {
            auto& v = vertices[i];
            const auto vj = mesh_json[int(i)];
            if(vj.isArray() && vj.size() == 2)
            {
              v.x = float(vj[0].asDouble());
              v.y = float(vj[1].asDouble());
            }
          }
        }
      }
    }
  }
}
