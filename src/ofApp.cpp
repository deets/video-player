#include "ofApp.h"

#include "nanomsg/nn.h"
#include "nanomsg/pair.h"

#include <cassert>

namespace {

template<typename F>
void onJsonKey(const char* key, const Json::Value& v, const F& f)
{
  if(v.isMember(key))
  {
    f(v[key]);
  }
}
auto FONT_SIZE = 30;
auto FONT_LINE_HEIGHT = 20.0;
auto FONT_LETTER_SPACING = 1.037;

auto FOREGROUND_TIMEOUT = std::chrono::seconds(30);

} // end ns anonymous

ofApp::ofApp(const ProjectionRect& rect, bool auto_foreground, bool full_screen, float fadeout)
  : ofBaseApp()
  , _mode(Mode::NORMAL)
  , _background(0, 0, 0, 255)
  , _text_color(0, 255, 0, 255)
  , _socket(AF_SP, NN_PAIR)
  , _player(1920, 960, rect, fadeout)
  , _calibrator(_player)
  , _is_fullscreen(full_screen)
  , _auto_foreground(auto_foreground)
{
}


//--------------------------------------------------------------
void ofApp::setup(){
  const auto uri = "ipc://magic-book";
  std::cout << "Binding to URI: " << uri << "\n";
  _socket.bind(uri);
  //    _socket.bind("ipc://videoplayer");
  ofSetFrameRate(60);

  ofTrueTypeFont::setGlobalDpi(72);

  overlay_font.load("type/verdana.ttf", FONT_SIZE, true, true);
  //  overlay_font.setLineHeight(FONT_LINE_HEIGHT);
  overlay_font.setLetterSpacing(FONT_LETTER_SPACING);

  _calibrator.load_configuration();

  full_screen(_is_fullscreen);
}

//--------------------------------------------------------------
void ofApp::update(){
  // process all messages first
  process_message();
  // now all sub-entities
  _player.update();
  auto_foreground();
}

//--------------------------------------------------------------
void ofApp::process_message()
{
  auto message = receive_message();
  if(message.size())
  {
    Json::Reader reader;
    Json::Value root;
    if(reader.parse(message, root))
    {
      process_json(root);
    }
    else
    {
      std::cerr << "message was undecipherable as JSON\n";
    }
  }
}


void ofApp::auto_foreground()
{
  if(_auto_foreground)
  {
    const auto now = std::chrono::steady_clock::now();
    if(now - _last_auto_foreground > FOREGROUND_TIMEOUT)
    {
      auto handle = ofGetWin32Window();
      BringWindowToTop(handle);
      SetForegroundWindow(handle);
      _last_auto_foreground = now;
    }
  }
}

//--------------------------------------------------------------
void ofApp::process_json(const Json::Value& json)
{
  onJsonKey("background", json, [this](const Json::Value& v) {
      assert(v.isArray() && v.size() == 3);
      _background.set(
          v[0].asInt(), v[1].asInt(), v[2].asInt()
      );
      std::cout << _background;
    }
  );

  onJsonKey("load-and-play", json, [this](const Json::Value& v) {
      std::vector<std::string> paths;
      if(v.isString())
      {
        paths.push_back(v.asString());
      }
      else
      {
        assert(v.isArray());
        for(const auto path : v)
        {
          paths.push_back(path.asString());
        }
      }
      for(const auto& path : paths)
      {
        std::cerr << "schedule video: " << path << "\n";
      }
      _player.load_and_play(paths);
    }
  );

  onJsonKey("stop", json, [this](const Json::Value& v) {
      std::cerr << "stop\n";
      _player.stop();
    }
  );

  onJsonKey("adjust-projection-rect", json, [this](const Json::Value& v) {
      _player.adjust_rect(ProjectionRect::fromJson(v));
    }
  );

  onJsonKey("set-player-enabled", json, [this](const Json::Value& v) {
      assert(v.isBool());
      _player.enabled(v.asBool());
    }
  );

    onJsonKey("center-text", json, [this](const Json::Value& v) {
      assert(v.isString());
      _center_text = v.asString();
    }
  );

  onJsonKey("full-screen", json, [this](const Json::Value& v) {
      assert(v.isBool());
      full_screen(v.asBool());
    }
  );

  onJsonKey("cover-mode", json, [this](const Json::Value& v) {
      assert(v.isBool());
      std::cerr << "cover-mode: " << v.asBool() << "\n";
      _player.mode(v.asBool() ? Player::COVER : Player::VIDEO);
    }
  );

}


void ofApp::full_screen(bool fs)
{
  ofSetFullscreen(fs);
  if(fs)
  {
    ofHideCursor();
  }
  else
  {
    ofShowCursor();
  }
  _is_fullscreen = fs;
}


//--------------------------------------------------------------
std::string ofApp::receive_message()
{
  std::string res;
  char* buffer;
  auto len = _socket.recv(&buffer, NN_MSG, NN_DONTWAIT);
  if(len > 0)
  {
    res.resize(len);
    res = std::string(buffer, buffer+len);
    nn_freemsg(buffer);
  }
  return res;
}

//--------------------------------------------------------------
void ofApp::draw()
{
  switch(_mode)
  {
  case Mode::NORMAL:
    ofBackground(_background);
    _player.draw();
    drawCenteredText();
    break;
  case Mode::CALIBRATE:
    _calibrator.draw();
    break;
  }
}


void ofApp::drawCenteredText()
{
  if(!_center_text.empty())
  {
    ofSetColor(_text_color);
    auto tx = ofGetWindowWidth() / 2;
    auto ty = ofGetWindowHeight() / 2;
    const auto bounds = overlay_font.getStringBoundingBox(_center_text, 0, 0);
    tx -= bounds.width / 2;
    ty -= bounds.height / 2;
    overlay_font.drawString(_center_text, tx, ty);
  }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
  std::cout << "keyPressed: " << key << "\n";
  switch(key)
  {
  case 'c':
    _mode = _mode == Mode::NORMAL ? Mode::CALIBRATE : Mode::NORMAL;
    break;
  case 'f':
    full_screen(!_is_fullscreen);
    break;
  case 'b':
    _background = _background == ofColor(0, 0, 0, 255) ? ofColor(255, 255, 255, 255) : ofColor(0, 0, 0, 255);
    break;
  case 'k':
    std::cout << "DYING!!!!!\n";
    int *p = 0;
    *p = 0; // Access violation
    break;
  }
  if(_mode == Mode::CALIBRATE)
  {
    _calibrator.keyPressed(key);
  }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
  std::cout << "keyReleased: " << key << "\n";
  if(_mode == Mode::CALIBRATE)
  {
    _calibrator.keyReleased(key);
  }
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
