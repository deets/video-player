#include "player.hpp"

PlayerAux::PlayerAux(const std::string& path, std::function<int()> alpha)
  : setup(false)
  , _path(path)
  , _alpha(alpha)
{
  player.load(path);
  player.setLoopState(OF_LOOP_NONE);
}


void PlayerAux::loop(bool do_loop)
{
  player.setLoopState(do_loop ? OF_LOOP_NORMAL : OF_LOOP_NONE);
}


void PlayerAux::play()
{
  std::cout << "play: " << _path << "\n";
  player.play();
}


void PlayerAux::set_trigger(std::function<void()> trigger)
{
  _trigger = trigger;
}


void PlayerAux::stop()
{
  player.stop();
}


void PlayerAux::update()
{
  player.update();
  if(abs(player.getTotalNumFrames() - player.getCurrentFrame()) <= 1  && _trigger)
  {
    _trigger();
    stop();
    _trigger = nullptr;
  }
}


void PlayerAux::draw(ofImage& image)
{
  if(!setup)
  {
    //      LOGGER()->debug("setup fbo");
    setup = true;
    auto videoWidth = player.getTexture().getWidth();
    auto videoHeight = player.getTexture().getHeight();
    fbo.allocate(videoWidth, videoHeight);
    // clear fbo
    fbo.begin();
    ofClear(255, 255, 255);
    fbo.end();
  }
  if (player.isFrameNew())
  {
    fbo.begin();
    ofClear(255, 255, 255, 255);
    ofEnableAlphaBlending();
    const auto a = _alpha();
    ofSetColor(255, 255, 255, a);
    // draw image into fboB
    player.draw(0, 0);
    player.setVolume(a / 255.0f);
    ofDisableAlphaBlending();
    fbo.end();
  }
  fbo.readToPixels(fboPixels);
  image.setFromPixels(fboPixels);
}


ProjectionRect ProjectionRect::fromJson(const Json::Value& v)
{
  ProjectionRect res;
  assert(v.isArray() && v.size() == 4);
  for(auto i=0; i < 4; ++i)
  {
    assert(v[i].isArray() && v[i].size() == 2);
    auto coord = v[i];
    res[i] = ofPoint(coord[0].asInt(), coord[1].asInt());
  }
  return res;
}


ofPoint& ProjectionRect::operator[](int index)
{
  switch(index)
  {
  case 0:
    return topleft;
  case 1:
    return topright;
  case 2:
    return bottomleft;
  case 3:
    return bottomright;
  default:
    throw std::runtime_error("tried to acces non existing coordinate");
  }
}


Player::Player(int videoWidth, int videoHeight, const ProjectionRect& rect, float fadeout)
  : _videoWidth(videoWidth)
  , _videoHeight(videoHeight)
  , _backgroundColor(255, 255, 255, 255)
  , _enabled(true)
  , _fadeout(fadeout)
  , _mode(Player::VIDEO)
{
  std::cout << "initialising video player with " << videoWidth << "x" << videoHeight << "\n";
  _mesh.addVertex(rect.topleft);
  _mesh.addTexCoord(ofPoint(0, 0));
  _mesh.addColor(ofColor(255, 255, 255));
  _mesh.addVertex(rect.topright);
  _mesh.addTexCoord(ofPoint(_videoWidth, 0));
  _mesh.addColor(ofColor(255, 255, 255));
  _mesh.addVertex(rect.bottomleft);
  _mesh.addTexCoord(ofPoint(0, _videoHeight));
  _mesh.addColor(ofColor(255, 255, 255));
  _mesh.addVertex(rect.bottomright);
  _mesh.addTexCoord(ofPoint(_videoWidth, _videoHeight));
  _mesh.addColor(ofColor(255, 255, 255));

  const auto x = 0;
  const auto y = 0;
  int i1 = x + 2 * y;
  int i2 = x+1 + 2 * y;
  int i3 = x + 2 * (y+1);
  int i4 = x+1 + 2 * (y+1);
  _mesh.addTriangle( i1, i2, i3 );
  _mesh.addTriangle( i2, i4, i3 );

  _image.allocate(_videoWidth, _videoHeight, OF_IMAGE_COLOR_ALPHA);
  _image.setColor(_backgroundColor);
  _image.update();

  _coverImage.allocate(_videoWidth, _videoHeight, OF_IMAGE_COLOR_ALPHA);

  for (int x = 0; x < _videoWidth; x++) {
    for (int y = 0; y < _videoHeight; y++) {
      const auto color = x < _videoWidth / 2 ?  ofColor::black :  ofColor::white;
      _coverImage.setColor(x, y, color);
    }
  }
  _coverImage.update();

}


Player::~Player()
{
  teardown();
}

bool Player::enabled() const
{
  return _enabled;
}


void Player::enabled(bool enabled)
{
  _enabled = enabled;
}


void Player::adjust_rect(const ProjectionRect& rect)
{
  _mesh.setVertex(0, rect.topleft);
  _mesh.setVertex(1, rect.topright);
  _mesh.setVertex(2, rect.bottomleft);
  _mesh.setVertex(3, rect.bottomright);
}


void Player::teardown()
{
  for(auto& player : _players)
  {
    player->stop();
  }
  _active = 0;
  _players.clear();
  _image.setColor(_backgroundColor);
  _image.update();
}


ofMesh& Player::mesh()
{
  return _mesh;
}


const ofMesh& Player::mesh() const
{
  return _mesh;
}


void Player::load_and_play(const std::vector<std::string>& paths)
{
  teardown();
  _active = 0;
  _fadeout_timestamp = 0.0f;
  _teardown_on_update = false;

  for(const auto& path : paths)
  {
    _players.push_back(std::make_unique<PlayerAux>(path, [this]() { return alpha(); }));
  }
  if(_players.size())
  {
    for(std::size_t i=0; i < _players.size() - 1; ++i)
    {
      auto* next_player = _players[i+1].get();
      _players[i]->set_trigger(
          [this, next_player]()
          {
            ++_active;
            next_player->play();
          }
      );
    }
    _players.back()->loop(true);
  }
  if(current())
  {
    current()->play();
  }
}


void Player::stop()
{
  if(_fadeout == 0.0f)
  {
    teardown();
  }
  else
  {
    fadeout();
  }
}

void Player::update()
{
  for(auto& player : _players)
  {
    player->update();
  }
  if(_teardown_on_update)
  {
    _teardown_on_update = false;
    teardown();
  }
}


PlayerAux* Player::current()
{
  if(_players.size() && _active < _players.size())
  {
    return _players[_active].get();
  }
  return nullptr;
}

void Player::mode(Mode mode)
{
  _mode = mode;
}


void Player::draw()
{
  if(!_enabled)
  {
    return;
  }

  // draw image to screen
  ofPushMatrix(); //Store the coordinate system
  //Move the coordinate center to screen's center
  ofTranslate( ofGetWidth()/2, ofGetHeight()/2, 0 );

  if(current())
  {
    current()->draw(_image);
  }

  ofImage *image = nullptr;
  switch(_mode)
  {
  case Player::VIDEO:
    image = &_image;
    break;
  case Player::COVER:
    image = &_coverImage;
    break;
  }
  assert(image);
  image->bind();
  _mesh.draw();
  image->unbind();
  ofPopMatrix(); //Restore the coordinate system
}



int Player::alpha()
{
  if(_fadeout == 0.0f || _fadeout_timestamp == 0.0f)
  {
    return 255;
  }
  const auto elapsed = ofGetElapsedTimef() - _fadeout_timestamp;
  int a = 255 - int((elapsed / _fadeout) * 255.0);
  if(a < 0) {
    a = 0;
    _teardown_on_update = true;
    _fadeout_timestamp = 0.0f;
  }
  return a;
}


void Player::fadeout()
{
  _fadeout_timestamp = ofGetElapsedTimef();
}
