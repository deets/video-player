#pragma once

#include "ofMain.h"

#include <json/json.h>

#include <memory>
#include <string>


class PlayerAux
{
public:
  PlayerAux(const std::string& path, std::function<int()> alpha);

  void loop(bool);

  void draw(ofImage&);
  void play();
  void stop();
  void update();

  void set_trigger(std::function<void()> trigger);

private:


  ofVideoPlayer player;
  bool setup;
  ofFbo fbo;
  ofPixels fboPixels;
  std::function<void()> _trigger;

  std::string _path;

  std::function<int()> _alpha;

};


struct ProjectionRect
{
  ofPoint topleft;
  ofPoint topright;
  ofPoint bottomleft;
  ofPoint bottomright;

  ofPoint& operator[](int);

  static ProjectionRect fromJson(const Json::Value&);
};


class Player
{
public:
  enum Mode {
    VIDEO,
    COVER
  };

  Player(int videoWidth, int videoHeight, const ProjectionRect&, float fadeout);

  ~Player();

  void adjust_rect(const ProjectionRect&);

  void load_and_play(const std::vector<std::string>& paths);
  void stop();
  void update();
  void draw();
  bool enabled() const;
  void enabled(bool);

  ofMesh& mesh();
  const ofMesh& mesh() const;

  void mode(Mode);

private:
  void teardown();
  int alpha();
  void fadeout();

  PlayerAux* current();

  std::vector<std::unique_ptr<PlayerAux>> _players;
  std::size_t _active;

  ofMesh _mesh;
  int _videoWidth, _videoHeight;
  // contains video frames
  ofImage _image;
  // contains half black/half white for
  // cover mode
  ofImage _coverImage;
  ofColor _backgroundColor;
  bool _enabled;

  // no fadeout if 0.0f
  float _fadeout;
  float _fadeout_timestamp;

  // we can't teardown in draw, so
  // we need a flag to signal the next
  // update that it should teardown the playr
  bool _teardown_on_update;

  Mode _mode;
};
