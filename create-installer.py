import os
import re
import shutil
import argparse
import tempfile
import zipfile
import glob
import subprocess
import pefile
from mako.template import Template


BASE = os.path.abspath(os.path.dirname(__file__))
DROPBOX = r"C:\Users\deets\Dropbox\magicbook"
BUILD = os.path.join(BASE, "bin")
ISS_FILE = os.path.join(BASE, "installer", "video-player.iss")

BLACKLIST = [
    'NETAPI32.DLL',
    'MSVCP90D.DLL',
    'MSVCP90.DLL',
    'MSVCR90D.DLL',
    'MSVCR90.DLL',
    'KERNEL32.DLL',
    'USER32.DLL',
    'ADVAPI32.DLL',
    'WS2_32.DLL',
    'NTDLL.DLL',
    'OLE32.DLL',
    'MSVCRT.DLL',
    'KERNELBASE.DLL',
    'GDI32.DLL',
    'COMDLG32.DLL',
    'SHLWAPI.DLL',
    'COMCTL32.DLL',
    'SHELL32.DLL',
    'FIREWALLAPI.DLL',
    'RPCRT4.DLL',
    'SSPICLI.DLL',
    'CRYPTBASE.DLL',
    'BCRYPTPRIMITIVES.DLL',
    'OLEAUT32.DLL',
    'COMBASE.DLL',
    'IMM32.DLL',
    'WINMM.DLL',
    'WINMMBASE.DLL',
    'WINSPOOL.DRV',
    'BCRYPT.DLL',
]


def version_h_defs():
    res = dict(
        VER_PRODUCTNAME_STR="video player",
        VER_PRODUCTVERSION_STR="1.0",
        VER_COMPANYNAME_STR="Diez B. Roggisch",
        VER_COMPANYURL_STR="http://roggisch.de",
        VER_PRODUCTFILENAME_STR="video-player.exe"
    )
    res["VER_VERSION_FILESAFE"] = res["VER_PRODUCTVERSION_STR"].replace(
        ".", "_"
    )
    return res


def find_dll_path(dllname, basepath):
    for path in [basepath] + os.environ["PATH"].split(";"):
        fullpath = os.path.join(path, dllname)
        if os.path.exists(fullpath):
            return fullpath
    raise Exception("No path found for {}".format(dllname))


def find_dependencies(loadable, basepath, found=set()):
    if loadable in found:
        return
    found.add(loadable)
    pe = pefile.PE(loadable)
    imports = pe.DIRECTORY_ENTRY_IMPORT
    for dep in imports:
        dll = dep.dll.decode("ascii")
        if dll.upper() in BLACKLIST or dll.upper().startswith("API-MS-WIN"):
            continue
        path = find_dll_path(dll, basepath)
        yield path
        yield from find_dependencies(path, basepath)


def generate_installer_script(all_files, all_images, all_types):
    assert os.path.exists(ISS_FILE), ISS_FILE
    vcredist_exe = os.path.join(BASE, "installer", "vcredist_x64.exe")
    assert os.path.exists(vcredist_exe), vcredist_exe
    tmpl = Template(filename=ISS_FILE)
    return tmpl.render(
        files=all_files,
        images=all_images,
        types=all_types,
        vcredist_exe=vcredist_exe,
        **version_h_defs(),
    )


def build(clean_build=True, config="Release"):
    assert os.path.exists(DROPBOX)

    exe = os.path.join(BUILD, "video-player.exe")
    # if clean_build and os.path.exists(BUILD):
    #     shutil.rmtree(BUILD)

    # if not os.path.exists(exe):
    #     os.mkdir(BUILD)
    #     subprocess.call(["cmake", "-DCMAKE_BUILD_TYPE={}".format(config), ".."], cwd=BUILD)
    #     subprocess.call(["cmake", "--build", ".", "--config", config,], cwd=BUILD)

    assert os.path.exists(exe)
    all_files = [exe] + list(find_dependencies(exe, os.path.dirname(exe)))
    images_path = os.path.join(BASE, "data", "images")
    assert os.path.exists(images_path)
    all_images = [os.path.join(images_path, image) for image in os.listdir(images_path)]
    types_path = os.path.join(BASE, "data", "type")
    assert os.path.exists(types_path)
    all_types = [os.path.join(types_path, image) for image in os.listdir(types_path)]
    iss = tempfile.mktemp()
    with open(iss, "wb") as outf:
        outf.write(generate_installer_script(all_files, all_images, all_types).encode("utf-8"))

    output = os.path.join(DROPBOX)
    subprocess.check_call(["iscc.exe", "/O{}".format(output), iss], cwd=BASE)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--clean", action="store_true")
    parser.add_argument("--configuration", choices=["Release", "Debug"], default="Release")
    opts = parser.parse_args()
    build(clean_build=opts.clean, config=opts.configuration)


if __name__ == "__main__":
    main()
